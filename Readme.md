# Awesome coop

A categorized list of cooperatives around the globe

--------------------

- List of cooperatives
  - [Tech](#tech)

--------------------

## Tech

Based on the list from: https://github.com/mttmyr/awesome-tech-cooperatives

__E-Commerce__
- [Fairmondo](https://fairmondo.uk)
- [Stocksy](https://www.stocksy.com)

__Ride Sharing__
- [Commuterz](https://www.commuterz.io)
- [LaZooz](http://lazooz.org)
- [Modo](http://modo.coop)

__Media / Social__
- [social.coop](https://social.coop)
- [Resonate](https://resonate.is)

__Healthcare__
- [healthbank.coop](https://www.healthbank.coop)
- [savvy.coop](http://savvy.coop)

__Projects / Open Source__
- [Loomio](https://www.loomio.org)

__Finance__
- [ArtsPool](http://artspool.co)

__Agencies / Consulting__
- [Agaric](http://agaric.coop/)
- [bits.coop](https://bits.coop)
- [caravan.coop](https://caravan.coop)
- [colab.coop](https://colab.coop)
- [Enspiral](https://enspiral.com)
- [FeelTrain](https://feeltrain.com)
- [Igalia](https://www.igalia.com)
- [Outlandish](https://www.outlandish.com)
- [Plausible](https://www.plausible.coop)
- [Sassafras](http://sassafras.coop)
- [TESA](http://www.toolboxfored.org)
- [Aptivate](http://aptivate.org/)
- [Autonomic](https://autonomic.zone/)
- [Developer Society](https://www.dev.ngo/)
